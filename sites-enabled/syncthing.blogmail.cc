server {
	listen 80;
	listen [::]:80;

	listen 443 ssl;
	listen [::]:443 ssl;

	server_name syncthing.blogmail.cc;
	
	client_max_body_size 50M;

	ssl_certificate /etc/letsencrypt/live/mail.blogmail.cc/fullchain.pem;
	ssl_certificate_key /etc/letsencrypt/live/mail.blogmail.cc/privkey.pem;

	# Letsencrypt email: al@blogmail.cc
	location /.well-known { root /var/www/ssl-proof/; }

	location / {
		#access_log off;
		proxy_set_header X-Real-IP $remote_addr;
		proxy_set_header Host $host;
		proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
		# Using a variable is just to avoid nginx crash at startup if it can't
		# resolve the target hostname
		# @see https://stackoverflow.com/q/32845674#comment61836356_32846603
		resolver 10.1.152.10 valid=300s;
		set $upstream_host 10.1.152.38:8384;
		proxy_pass https://$upstream_host$request_uri;
	}
}
